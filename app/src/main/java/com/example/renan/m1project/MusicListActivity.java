package com.example.renan.m1project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MusicListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);


    }

    public void onClick(View view) {
        int songID = 1;
        Intent intent = new Intent(this, SongPlayingActivity.class);
        intent.putExtra("songID", songID);
        startActivity(intent);
    }
}
